﻿using EmployeeMangementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Repositories.EmployeeRepo
{
    interface IEmployeeRepository
    {
        IList<Employee> GetAllEmplyoyee();
        void AddEmployyee(Employee e);
        void UpdateEmployee(int id);
        int DeleteEmployee(int id);
    }
}
