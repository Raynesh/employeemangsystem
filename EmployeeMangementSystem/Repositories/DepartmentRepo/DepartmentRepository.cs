﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EmployeeMangementSystem.Context;
using EmployeeMangementSystem.Models;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace EmployeeMangementSystem.Repositories.DepartmentRepo
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private IDbConnection db;
    
        public DepartmentRepository(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            this.db = new SqlConnection(configuration.GetConnectionString("Database"));

        }

        public bool AddDepartment(Department d)
        {
            var sql = " insert into Department (DepartmentId, DepartmentName) Values (@DepartmentId,@DepartmentName) ;" +
                "Select cast(scope_identity() as int);";
            var id = db.Query<int>(sql,d).Single();
            d.DepartmentId = id;
            return true;
        }

        public void DeleteDepartment(int id)
        {
            var sql = "Delete from Department where DepartmentId = @id";
            db.Execute(sql, new { id });
        }

        public Department Get(int id)
        {
            var sql = "Select * from Department where DepartmentId=@DepartmentId";
            return db.Query<Department>(sql, new { @DepartmentId = id }).Single();
        }

        public IList<Department> GetAllDepartment()
        {
            var sql = "Select * from Department";
            return db.Query<Department>(sql).ToList();
        }

        public void UpdateDepartment(Department departmentupdates)
        {
            var sql = "Update Department Set DepartmentName = @DepartmentName where DepartmentId = @DepartmentId";
            db.Execute(sql, departmentupdates);


        }

    
    }
}
