﻿using EmployeeMangementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Repositories.DepartmentRepo
{
    public interface IDepartmentRepository
    {
        IList<Department> GetAllDepartment();
        Department Get(int id);
        bool AddDepartment(Department d);
        void UpdateDepartment(Department departmentupdates);
        void DeleteDepartment(int id);
    }
}
