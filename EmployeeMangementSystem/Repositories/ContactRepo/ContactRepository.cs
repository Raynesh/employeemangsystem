﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeMangementSystem.Context;
using EmployeeMangementSystem.Models;

namespace EmployeeMangementSystem.Repositories.ContactRepo
{
    public class ContactRepository : IContactRepository
    {
        readonly EmployeeContext _contactContext;
        public ContactRepository(EmployeeContext context )
        {
            _contactContext = context;

        }
       
        public void add(Contact newcontact)
        {
            throw new NotImplementedException();
        }

        public Contact delete(int id)
        {
           Contact contact= _contactContext.Contacts.Find(id);
            if (contact != null)
            {
                _contactContext.Contacts.Remove(contact);
                _contactContext.SaveChanges();
             }
            return contact;
        }

        public Contact Get(int id)
        {
            return _contactContext.Contacts.FirstOrDefault(c => c.ContactId == id);
            
        }

        public IEnumerable<Contact> GetAll()
        {
           return _contactContext.Contacts.ToList();
        }

        public void update(Contact contactchanges)
        {
            throw new NotImplementedException();
        }
    }
}
