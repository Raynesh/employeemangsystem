﻿using EmployeeMangementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Repositories.ContactRepo
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetAll();
        Contact Get(int id);
        void add(Contact newcontact);
        Contact delete(int id);
        void update(Contact contactchanges);
    }
}
