﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeMangementSystem.Models;
using EmployeeMangementSystem.Repositories.ContactRepo;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeMangementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : Controller
    {
        private readonly IContactRepository _contactRepository;
        public ContactController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        [HttpGet("getallcontacts")]
        public IActionResult Get()
        {
            IEnumerable<Contact> contacts = _contactRepository.GetAll();
            return Ok(contacts);

        }

        [HttpDelete("deletecontact/{id}")]
        public void Delete(int id)
        {
            _contactRepository.delete(id);
        }
       
    }
}