﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Models
{
    public class EmployeeDepartment
    {
        [Key]
        public int EmplyoyeeDepartmentId { get; set; }


        [ForeignKey("EmployeeId")]
        public virtual Employee Emplyoyees { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Departments { get; set; }
    }
}
