﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Models
{
    public class EmployeeProject
    {
        [Key]
        public int EmplyoyeeProjectId { get; set; }


        [ForeignKey("EmployeeId")]
        public virtual Employee Emplyoyees { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
    }
}
