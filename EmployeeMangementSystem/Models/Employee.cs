﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        public virtual int? ContactId { get; set; }
        public virtual int? DepartmentId { get; set; }
        public virtual int? ProjectId { get; set; }
        public bool IsWorking { get; set; }
        public decimal Salary { get; set; }
        public string Designation { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contacts { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Departments { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
    }
}
