﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMangementSystem.Models
{
    public class EmplyoyeeContact
    {
        [Key]
        public int EmplyoyeeContactId { get; set; }


        [ForeignKey("EmployeeId")]
        public virtual Employee Emplyoyees { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contacts { get; set; }
    }
}
