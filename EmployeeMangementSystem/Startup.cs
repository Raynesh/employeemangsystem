﻿using EmployeeMangementSystem.Context;
using EmployeeMangementSystem.Repositories.ContactRepo;
using EmployeeMangementSystem.Repositories.DepartmentRepo;
using EmployeeMangementSystem.Repositories.EmployeeRepo;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
namespace EmployeeMangementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Enable Cors
            services.AddCors(
                c =>
                {
                    c.AddPolicy("AllowOrigin",options=>options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
                }
                );

            
            
            



            services.AddDbContext<EmployeeContext>(
            options=>options.UseSqlServer(Configuration.GetConnectionString("Database")));
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();

            //JSON Serializer
            services.AddMvc().AddJsonOptions(o => o.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver()); ;


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", 
                    new Microsoft.OpenApi.Models.OpenApiInfo
                    {           
                    Version = "V1",
                    Title = "Employee Management API",
                    Description="API for manipulating Company Data"
                    });
               
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();

       

            app.UseSwaggerUI(C=> {
                C.SwaggerEndpoint("/swagger/v1/swagger.json","Employee APIs");
                
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
